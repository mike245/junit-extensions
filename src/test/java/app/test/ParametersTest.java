package app.test;

import app.test.params.JsonArgumentsProvider;
import app.test.params.VariableSource;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Inspired by https://www.baeldung.com/author/ali-dehghani/ work
 */
public class ParametersTest {

    public static final Stream<Arguments> args = Stream.of(
            Arguments.of(null, true, new Supplier<String>() {
                @Override
                public String get() {
                    return ParametersTest.getTodayDate("mm:ss ddMMyyyy");
                }
            }),
            Arguments.of("", true, (Supplier<String>)() -> ParametersTest.getTodayDate("mm:ss ddMMyyyy"))
//            Arguments.of("  ", true, ParametersTest.getTodayDate("mm:ss ddMMyyyy")),
//            Arguments.of("not blank", false, ParametersTest.getTodayDate("mm:ss ddMMyyyy"))
    );

    public static final Stream<Arguments> varargs = Stream.of(
            Arguments.of("a", true),
            Arguments.of("aa", true),
            Arguments.of("boris", true),
            Arguments.of("bba", false)
    );

    @ParameterizedTest
    @MethodSource("stringsProvider")
    void isBlank_ShouldReturnTrueForNullOrBlankStrings(String input, boolean expected) {
        assertEquals(expected, ProdClass.isBlank(input));
    }

    @ParameterizedTest
    @VariableSource("args")
    void isBlank_ShouldReturnTrueForNullOrBlankStringsVar(String input, boolean expected, Supplier<String> dateSupplier) {
        System.out.println(dateSupplier.get());
        assertEquals(expected, ProdClass.isBlank(input));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @ParameterizedTest
    @VariableSource("varargs")
    void testA(String input, boolean expected) {
        System.out.println("testA "+input);
        assertEquals(expected, ProdClass.shorterThan3(input));
    }

    @ParameterizedTest
    @VariableSource("varargs")
    void testBoris(String input, boolean expected) {
        System.out.println("testB "+input);
        assertEquals(expected, ProdClass.shorterThan3(input));
    }

    @ParameterizedTest

    @ArgumentsSource(JsonArgumentsProvider.class)
    void testExternalProvider(User user) {
        assertTrue(user.getAge() > 0);
    }

    private static Stream<Arguments> stringsProvider() {
        return args;
    }

    public static String getTodayDate(String format) {
        return LocalDateTime.now(ZoneId.of("America/Curacao")).format(DateTimeFormatter.ofPattern(format).withLocale(Locale.ENGLISH));
    }
}
