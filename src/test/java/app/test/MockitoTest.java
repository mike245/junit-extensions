package app.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

public class MockitoTest {

    private ClassA testedInstance;
    @Mock
    private ClassB b;
    @Mock
    private DbSaver saver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        testedInstance = new ClassA(b, saver);
    }

    @Test
    void testCreateA() {

        // return value & no params
        when(b.createB()).thenReturn("C");
        when(b.createB()).thenCallRealMethod();
        when(b.createInt()).thenReturn(-50);

        // return value & params
        when(b.createBoolean(eq(50))).thenReturn(false);
        when(b.createBoolean(eq(-50))).thenReturn(true);

        when(b.createBoolean(anyInt())).thenAnswer((Answer<Boolean>) invocationOnMock -> {
            final int argument = invocationOnMock.getArgument(0);
            return argument <= 100;
        });


        // no return value & no params

//        doCallRealMethod().when(b).voidMethod();
//        doThrow(new RuntimeException()).when(b).voidMethod();
        doAnswer(invocationOnMock -> {
            System.out.println("Good bye");
            return null;
        }).when(b).voidMethod();

        final String actual = testedInstance.createString(57);

        assertEquals("aB-50true", actual);
    }
}
