package app.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AssumptionTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void testAssumeTrue() {
        Assumptions.assumeTrue(Boolean.valueOf(System.getProperty("DEVAAA")));

        assertTrue(1 == 1);
    }

    @Test
    void testAssumingThat() {
        Assumptions.assumingThat(() -> {
            try {
                final CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, User.class);
                List<User> users = objectMapper.readValue(this.getClass().getClassLoader().getResource("data.json"), collectionType);
                return users.stream().anyMatch(u -> u.getAge() > 25);
            } catch (Exception e) {
                return false;
            }
        }, this::hugeTest);
    }

    private int hugeTest() {
        ///...
        assertFalse(1 == 1);
        return 0;
    }
}
