package app.test;

import app.test.lifecycle.LifecycleExtension;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(LifecycleExtension.class)
public class LifecycleTest {

    @BeforeAll
    static void beforeAll() {
        System.out.println("BeforeAll called");
        throw new RuntimeException();
//        fail();
    }

    @BeforeEach
    void setUp() {
//        fail();
        System.out.println("BeforeEach called");
//        throw new RuntimeException();
    }

    @AfterEach
    void tearDown() {
        System.out.println("AfterEach called");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("AfterAll called");
    }

    @Test
    void test() {
        System.out.println("Test called");
        assertTrue(true);
    }
}
