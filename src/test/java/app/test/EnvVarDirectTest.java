package app.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnvVarDirectTest {

    private String targetFile;

    @BeforeEach
    void setUp() {
        targetFile = System.getenv("targetConfig");
    }

    @Test
    void test() throws IOException {
        final Properties properties = new Properties();
        properties.load(Objects.requireNonNull(this.getClass().getClassLoader().getResource(targetFile+".properties")).openStream());

        final String actual = (String) properties.get("app.property");
        assertNotNull(actual);
        assertTrue(targetFile.contains(actual));
    }
}
