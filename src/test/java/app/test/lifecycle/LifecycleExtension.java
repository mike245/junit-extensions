package app.test.lifecycle;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.LifecycleMethodExecutionExceptionHandler;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

public class LifecycleExtension implements
        LifecycleMethodExecutionExceptionHandler,
        BeforeTestExecutionCallback,
        AfterAllCallback,
        TestExecutionExceptionHandler {

    private boolean beforeEachSuccess = true;

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        System.out.println("After all aftermethods");
    }

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        System.out.println("BeforeEach success");
    }

    @Override
    public void handleBeforeAllMethodExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        System.out.println("BeforeAll failure");
        throwable.printStackTrace();
        throw throwable;
    }

    @Override
    public void handleBeforeEachMethodExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        System.out.println("BeforeEach failure");
        throwable.printStackTrace();
        throw throwable;
    }

    @Override
    public void handleAfterEachMethodExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        throwable.printStackTrace();
        throw throwable;
    }

    @Override
    public void handleAfterAllMethodExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        throwable.printStackTrace();
        throw throwable;
    }

    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {

    }
}
