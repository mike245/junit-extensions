package app.test.parallel;

import app.test.parallel.a.ATest;
import app.test.parallel.b.BTest;
import app.test.parallel.c.CTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectClasses({ATest.class, CTest.class})
public class ACSuiteTest {
}
