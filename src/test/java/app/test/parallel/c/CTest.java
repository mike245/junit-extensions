package app.test.parallel.c;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CTest {

    @Test
    void testC() throws Exception {
        Thread.sleep(5000);
        assertTrue("c".equalsIgnoreCase("C"));
    }
}
