package app.test.parallel;

import app.test.parallel.a.ATest;
import app.test.parallel.b.BTest;
import app.test.parallel.c.CTest;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@Execution(ExecutionMode.SAME_THREAD)
@SelectPackages({"app.test.parallel.a", "app.test.parallel.b", "app.test.parallel.c"})
public class ParallelSuiteTest {


}
