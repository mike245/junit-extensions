package app.test.parallel.b;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BTest {

    @Test
    void testB() throws Exception {
        Thread.sleep(5000);
        assertTrue("a".equals("b"));
    }
}
