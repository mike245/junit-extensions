package app.test.parallel.a;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ATest {

    @Test
    void testA() throws Exception {
        Thread.sleep(5000);
        assertTrue("a".equals("a"));
    }
}
