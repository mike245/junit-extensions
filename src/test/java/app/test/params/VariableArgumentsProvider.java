package app.test.params;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.support.AnnotationConsumer;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class VariableArgumentsProvider implements ArgumentsProvider, AnnotationConsumer<VariableSource> {

    private String variableName;

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
//        if (testClass.isPresent()) {
//            final Class<?> aClass = testClass.get();
//            final Field field = this.getField(aClass);
//            final Stream<Arguments> value = this.getValue(field);
//            return value;
//        } else {
//            throw new IllegalArgumentException("");
//        }
//        final Optional<Class<?>> testClass = context.getTestClass();
//        final Optional<Field> field = testClass.map(c -> this.getField(c));
//        final Optional<Stream<Arguments>> argumentsStream = field.map(f -> this.getValue(f));
//        final Stream<Arguments> result = argumentsStream.orElseThrow(() ->
//                new IllegalArgumentException("Failed to load test arguments"));
//        return result;
        final Stream<Arguments> stream = context.getTestClass()
                .map(this::getField)
                .map(this::getValue)
                .orElseThrow(() ->
                        new IllegalArgumentException("Failed to load test arguments"));
        final Optional<Method> testMethod = context.getTestMethod();
        final Optional<String> name = testMethod.map(new Function<Method, String>() {
            @Override
            public String apply(Method method) {
                return method.getName();
            }
        });
        final Optional<String> lowerName = name.map(new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.toLowerCase();
            }
        });
        final String lowerTestName = lowerName.orElseThrow();
//        final Optional<Character> character = lowerName.map(new Function<String, Character>() {
//            @Override
//            public Character apply(String s) {
//                return s.toCharArray()[s.length() - 1];
//            }
//        });
//        String s = "hello";
//        final int length = s.length();
//        final char[] chars = s.toCharArray();
        // new char[]{'h', 'e', 'l', 'l', 'o'};

//        final Character lastLetter = character.orElseThrow();
//        final Character lastLetter = context.getTestMethod()
//                .map(Method::getName)
//                .map(String::toLowerCase)
//                .map(s -> s.toCharArray()[s.length() - 1])
//                .orElseThrow();
//        return stream.filter(a -> ((String) a.get()[0]).startsWith(lastLetter.toString()));
        return stream.filter(new Predicate<Arguments>() {
            @Override
            public boolean test(Arguments a) {
                final String s = (String) a.get()[0];
                return lowerTestName.contains(s);
            }
        });
    }

    @Override
    public void accept(VariableSource variableSource) {
        variableName = variableSource.value();
    }

    private Field getField(Class<?> clazz) {
        try {
            return clazz.getDeclaredField(variableName);
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private Stream<Arguments> getValue(Field field) {
        Object value = null;
        try {
            value = field.get(null);
        } catch (Exception ignored) {}

        return value == null ? null : (Stream<Arguments>) value;
    }
}
