package app.test.params;

import app.test.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class JsonArgumentsProvider implements ArgumentsProvider {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        final CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, User.class);
        List<User> users =  objectMapper.readValue(this.getClass().getClassLoader().getResource("data.json"), collectionType);
        return users.stream().map(Arguments::of);
    }
}
