package app.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CollectionTest {

    @Test
    public void test() {
        List<String> a = List.of("a", "b", "c");
        List<String> b = List.of("a", "d", "e");

        final ArrayList<String> tmp = new ArrayList<>(a);
        tmp.removeAll(b);

        Assertions.assertNotEquals(a.size(), tmp.size());
    }

    @Test
    void testLambda() {
        Set<String> a = new HashSet<>(Arrays.asList("a", "b", "c"));
        List<WebElement> b = List.of(new WebElement("a"), new WebElement("d"), new WebElement("e"));

        final Set<String> bNames = b.stream().map(WebElement::getName).collect(Collectors.toSet());

        final Optional<String> first = a.stream().filter(bNames::contains).findFirst();
        final Optional<String> ff = a.stream().filter(e -> bNames.contains(e)).findFirst();
        final Optional<WebElement> fff = b.stream().filter(e -> a.contains(e.getName())).findFirst();
        if (fff.isPresent()) {
            System.out.println(fff.get().getName());
        }

//        for (WebElement webElement : b) {
//            if (a.contains(webElement.getName())) {
//                return webElement;
//            }
//        }

//        WebElement d;
//        d.doSomething("", new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                return false;
//            }
//        });

//        d.doSomething("", e -> false);
//        d.doSomething2("", a::add);

        assertTrue(first.isPresent());
        assertEquals("a", first.get());
    }

    static class WebElement {

        private String name;

        public WebElement(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void doSomething(String a, Predicate<String> p) {

        }

        public void doSomething2(String a, Consumer<String> p) {

        }
    }
}
