package app.test.listener;

import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;

public class CustomListener implements TestExecutionListener {

    @Override
    public void executionStarted(TestIdentifier testIdentifier) {
        System.out.printf("Executing: %s%n", testIdentifier.getDisplayName());
    }
}
