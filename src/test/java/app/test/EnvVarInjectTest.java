package app.test;

import app.test.env.EnvVarExtension;
import app.test.env.InjectEnvVars;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@InjectEnvVars(
        value = {
                @InjectEnvVars.Inject(
                        field = "targetFile",
                        env = "targetConfig"
                ),
                @InjectEnvVars.Inject(
                        field = "stepName",
                        env = "USER"
                )
        }
)
public class EnvVarInjectTest {

    private String targetFile;
    private String stepName;

    @Test
    void test() throws IOException {
        final Properties properties = new Properties();
        properties.load(Objects.requireNonNull(this.getClass().getClassLoader().getResource(targetFile + ".properties")).openStream());

        final String actual = (String) properties.get("app.property");
        assertNotNull(actual);
        assertTrue(targetFile.contains(actual));
    }

}
