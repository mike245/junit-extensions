package app.test.env;

import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(EnvVarExtension.class)
public @interface InjectEnvVars {

    Inject[] value();

    @interface Inject {
        String field();
        String env();
    }
}
