package app.test.env;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;
import org.junit.platform.commons.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

import static org.junit.platform.commons.util.AnnotationUtils.findAnnotation;

public class EnvVarExtension implements TestInstancePostProcessor {
    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext context) throws Exception {
        final Optional<InjectEnvVars> annotation = findAnnotation(testInstance.getClass(), InjectEnvVars.class);
        if (annotation.isPresent()) {
            final InjectEnvVars.Inject[] variablesToInject = annotation.get().value();
            for (InjectEnvVars.Inject injection : variablesToInject) {
                final List<Field> fields = ReflectionUtils.findFields(
                        testInstance.getClass(),
                        f -> f.getName().equals(injection.field()),
                        ReflectionUtils.HierarchyTraversalMode.TOP_DOWN
                );
                for (Field field : fields) {
                    field.setAccessible(true);
                    field.set(testInstance, System.getenv(injection.env()));
                }
            }
        }
    }
}
