package app.test;

import java.util.concurrent.ThreadLocalRandom;

public class ProdClass {

    public boolean isNegative(int a) {
        return ThreadLocalRandom.current().nextBoolean();
    }

    public static boolean isBlank(String s) {
        return s == null || s.replaceAll(" ", "").length() == 0;
    }

    public static boolean shorterThan3(String s) {
        return s != null && s.length() < 3;
    }
}
