package app.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class CollectionExamples {

    public static void main(String[] args) {
//        lists();
//
//        args.hashCode()
//
//        List<String> a = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            a.add("a");
//        }
//
//        System.out.println(a.size());
//
//        Set<String> b = new HashSet<>();
//        for (int i = 0; i < 5; i++) {
//            b.add("a");
//        }
//
//        System.out.println(b.size());

        User a = new User("a", 25, Collections.emptySet());
        User b = new User("b", 21, Collections.emptySet());

        System.out.println(a.equals(b));
        System.out.println(a.hashCode() == b.hashCode());

        Set<User> users = new HashSet<>();
        users.add(a);
        users.add(b);
        System.out.println(users.size());
//
//        HashSet<Integer> a = new HashSet<Integer>();
//        a.add(1);
//        a.add(9);
//        a.add(6);
//        a.add(65);
//        a.add(45);
//        for (Integer integer : new HashSet<>(a)) {
//            System.out.println(integer);
//        }
//
//        TreeSet<Integer> b = new TreeSet<Integer>(Integer::compareTo);
//        b.add(1);
//        b.add(9);
//        b.add(6);
//        b.add(64);
//        b.add(45);
//        for (Integer integer : b) {
//            System.out.println(integer);
//        }

//        TreeSet<String> s = new TreeSet<String>((a, b) -> -Integer.compare(a.length(), b.length()));
//        s.add("a");
//        s.add("cc");
//        s.add("bbb");
//        for (String s1 : s) {
//            System.out.println(s1);
//        }


//        Map<String, Object> map = new HashMap<>();
//        map.put("a", 1);
//        map.put("b", 2);
//        System.out.println(map.get("a"));

//        for (String s : map.keySet()) {
//            System.out.println(s);
//            System.out.println(map.get(s));
//        }
//
//        for (Map.Entry<String, Object> entry : map.entrySet()) {
//            System.out.println(entry.getKey());
//            System.out.println(entry.getValue());
//        }
//
//        for (Object value : map.values()) {
//
//        }

//        Map<String, Object> treeMap = new TreeMap<>();
//        treeMap.put("a", 1);
//        treeMap.put("c", 2);
//        treeMap.put("b", 3);
//        for (String s : treeMap.keySet()) {
//            System.out.println(s + " " + treeMap.get(s));
//        }

//        Map<String, Object> treeMap = new TreeMap<>(new Comparator<String>() {
//            @Override
//            public int compare(String a, String b) {
//                return -Integer.compare(a.length(), b.length());
//            }
//        });
//        treeMap.put("a", 1);
//        treeMap.put("cc", 2);
//        treeMap.put("bbb", 3);
//        for (String s : treeMap.keySet()) {
//            System.out.println(s + " " + treeMap.get(s));
//        }

//        Map<String, Integer> map = new HashMap<>();
//        map.put(null, 1);
//        System.out.println(map.size());
//        System.out.println(map.get(null));
//        map.put("a", null);
//        System.out.println(map.get("b"));
//        final Integer b = map.getOrDefault("b", 500);
//        map.computeIfAbsent("b", k -> ThreadLocalRandom.current().nextInt());
//        System.out.println(map.get("b"));
    }

    private static void lists() {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 4, 12, 9, 198));

        list.contains(12); // O(n)

        list.sort(Integer::compareTo);
        list.sort((a, b) -> {
            return a.compareTo(b);
        });
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer a, Integer b) {
                return a.compareTo(b);
            }
        });

        Arrays.binarySearch(list.toArray(), 12); // O(logn)
    }
}
