package app.test;

public class ClassB {

    public int a = 2;

    public String createB() {
        return "B";
    }

    public int createInt() {
        return 100;
    }

    public boolean createBoolean(int a) {
        return a > 0;
    }

    public void voidMethod() {
        System.out.println("Hello");
    }
}
