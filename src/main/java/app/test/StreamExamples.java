package app.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExamples {

    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User("Adam", 25, Set.of("a", "b")),
                new User("Bob", 30, Set.of("c", "b")),
                new User("Ewan", 30, Set.of("c", "b")),
                new User("Charlie", 17, Set.of("a", "d")),
                new User("Dylan", 53, Set.of("z", "e"))
        );

        final List<String> uniqueFriends = users.stream()
                .filter(e -> e.getAge() > 20)
                .flatMap(e -> e.getFriends().stream())
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        for (String transformedUser : uniqueFriends) {
            System.out.println(transformedUser);
        }

        System.out.println(users.stream().filter(e -> e.getAge() > 20).count());
        IntStream s = IntStream.of(1, 2, 3);
        final OptionalInt max = users.stream().mapToInt(e -> e.getAge()).max();
        final Optional<Integer> maxA = users.stream().map(e -> e.getAge()).max(Integer::compareTo);
        final OptionalInt min = users.stream().mapToInt(e -> e.getAge()).min();
        final OptionalDouble avg = users.stream().mapToInt(e -> e.getAge()).average();

        final boolean flag = users.stream().anyMatch(e -> e.getAge() < 20);
        final boolean flag1 = users.stream().allMatch(e -> e.getAge() < 90);
        final boolean flag2 = users.stream().noneMatch(e -> e.getAge() > 90);

        final Optional<User> first = users.stream().filter(e -> e.getAge() < 20).findFirst();
        final Optional<User> any = users.stream().findAny();

        final Map<Integer, List<User>> groups = users.stream().collect(Collectors.groupingBy(User::getAge));
        final Map<Integer, List<User>> groupsA = new HashMap<>();
        for (User user : users) {
            if (groupsA.containsKey(user.getAge())) {
                groupsA.get(user.getAge()).add(user);
            } else {
                final ArrayList<User> userArrayList = new ArrayList<>();
                userArrayList.add(user);
                groupsA.put(user.getAge(), userArrayList);
            }
        }

        final Map<Boolean, List<User>> parts = users.stream().collect(Collectors.partitioningBy(u -> u.getAge() < 28));
        final List<User> under28 = parts.get(true);
        final List<User> above28 = parts.get(false);
//        System.out.println(transformedUsers);


//        final List<String> collect = Stream.of("a", "a", "a").distinct().collect(Collectors.toList());
//        for (String s : collect) {
//            System.out.println(s);
//        }
    }
}
