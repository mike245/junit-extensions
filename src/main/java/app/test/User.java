package app.test;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class User implements Comparable<User> {

    private String name;
    private int age;
    private Set<String> friends = new HashSet<>();

    public User() {
    }

    public User(String name, int age, Set<String> friends) {
        this.name = name;
        this.age = age;
        this.friends = friends;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<String> getFriends() {
        return friends;
    }

    public void setFriends(Set<String> friends) {
        this.friends = friends;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
//        return 0;
                return Objects.hash(name, age);
    }

    @Override
    public int compareTo(User user) {
        return 0;
    }
}
