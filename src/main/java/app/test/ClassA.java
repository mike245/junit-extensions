package app.test;

public class ClassA {

    private final ClassB b;
    private final DbSaver saver;

    public ClassA(ClassB b, DbSaver saver) {
        this.b = b;
        this.saver = saver;
    }

    public String createString(int x) {
        b.voidMethod();
        return "a" + b.createB() + b.createInt() + b.createBoolean(x);
    }

    public void createUser() {
        User u = new User();
        u.setAge(500);
        u.setName("Aaaa");

        saver.save(u);
    }
}
